import React, { Component } from "react";
import Header from "./header";
import SearchBar from "./search";
import ListConatiner from "./listContainer";
import Data from "../data.json";

class Main extends Component {
  state = {
    emoji: Data,
    emojiList: Data,
  };

  handleSearch = (text) => {
    if (text !== "") {
      this.setState((prevState) => {
        return {
          emoji: prevState.emoji.filter((e) =>
            e.title.toLowerCase().includes(text.toLowerCase())
          ),
        };
      });
    } else {
      this.setState({
        emoji: this.state.emojiList,
      });
    }
  };

  render() {
    return (
      <div>
        <Header emojiData={this.state.emoji} />
        <SearchBar searchEmoji={this.handleSearch} />
        <ListConatiner emojiData={this.state.emoji} />
      </div>
    );
  }
}

export default Main;
