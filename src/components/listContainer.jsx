import React, { Component } from "react";
import "../styles/listContainer.css";

class ListConatiner extends Component {
  render() {
    return (
      <div>
        {this.props.emojiData.map((emoji) => {
          return (
            <div className="emoji" key={emoji.title}>
              {emoji.symbol} {emoji.title}
            </div>
          );
        })}
      </div>
    );
  }
}

export default ListConatiner;
