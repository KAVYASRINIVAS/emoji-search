import React, { Component } from "react";
import "../styles/header.css";

class Header extends Component {
  render() {
    return (
      <div className="header">
        {this.props.emojiData.map((emoji) =>
          emoji.title === "Smile Cat" ? <span>{emoji.symbol}</span> : ""
        )}
        Emoji Search
        {this.props.emojiData.map((emoji) =>
          emoji.title === "Smiley Cat" ? <span>{emoji.symbol}</span> : ""
        )}
      </div>
    );
  }
}

export default Header;
