import React, { Component } from "react";
import "../styles/search.css";

class SearchBar extends Component {
  render() {
    return (
      <input
        type="text"
        className="search"
        onChange={(e) => {
          e.preventDefault();
          return this.props.searchEmoji(e.target.value);
        }}
      />
    );
  }
}

export default SearchBar;
